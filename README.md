# Game 2048
A react implementation of the popular [2048](https://github.com/gabrielecirulli/2048) game.

In this version of the game you can choose the board size and going back or forward in your moves.

## Screenshot

<p align="center">
  <img src="https://i.imgur.com/EBmPIDW.png" alt="Screenshot of Game-2048"/>
</p>

The screenshot is showing a game with 6x6 board.

## How to run
1. Clone the project and navigate to the main directory
2. `$ npm i` - install the dependencies
3. `$ npm start` - start the development version of the game and you can access the game on port [3000](http://localhost:3000)
4. `$ npm run build` - build the production version of the app which will be generated in the `build` folder. The content there can be deployed to a simple static server
## License
Game-2048 is licensed under the MIT license.
