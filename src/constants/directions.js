export const LEFT = 'LEFT';
export const RIGHT = 'RIGHT';
export const UP = 'UP';
export const DOWN = 'DOWN';

export const MOVING_DIRECTIONS = [
  LEFT, RIGHT, UP, DOWN
];

export const keyboardArrowCodes = {
  37: LEFT,
  38: UP,
  39: RIGHT,
  40: DOWN
};

export const ROW = 'ROW';
export const COL = 'COL';
