// Initialize actions
export const INITIAL_LOAD = 'INITIAL_LOAD';
export const LOAD_SETTINGS = 'LOAD_SETTINGS';
export const RESTORE_GAME = 'RESTORE_GAME';

// Game actions
export const START_GAME = 'START_GAME';
export const NEW_TILE = 'NEW_TILE';
export const USER_TURN = 'USER_TURN';
export const UPDATE_BOARD = 'UPDATE_BOARD';
export const USER_WON = 'USER_WON';
export const USER_LOSE = 'USER_LOSE';
export const USER_CONTINUE_PLAYING = 'USER_CONTINUE_PLAYING';
export const REPLACE_BOARD = 'REPLACE_BOARD';
export const CHANGE_GRID_SIZE = 'CHANGE_GRID_SIZE';

// History actions
export const LOAD_STATES = 'LOAD_STATES';
export const STORE_STATE = 'STORE_STATE';
export const UNDO_STEP = 'UNDO_STEP';
export const REDO_STEP = 'REDO_STEP';
