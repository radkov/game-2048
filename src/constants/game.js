export const WIN_NUMBER = 2048;

export const STARTED = 'STARTED';
export const RESTORED = 'RESTORED';
export const PLAYING = 'PLAYING';
export const WON = 'WON';
export const LOSE = 'LOSE';
export const CONTINUE_PLAYING = 'CONTINUE_PLAYING';

export const GAME_STATES = {
  STARTED,
  RESTORED,
  PLAYING,
  WON,
  LOSE,
  CONTINUE_PLAYING
};

export const GRID_SIZE_4 = 4;
export const GRID_SIZE_5 = 5;
export const GRID_SIZE_6 = 6;
