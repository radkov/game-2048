import { START_GAME, UPDATE_BOARD, CHANGE_GRID_SIZE } from './actionTypes';

export const GAME_SETTINGS = 'GAME_SETTINGS';
export const GAME_HISTORY = 'GAME_HISTORY';
export const MODIFYING_ACTIONS = [START_GAME, UPDATE_BOARD, CHANGE_GRID_SIZE];
export const PROPS_TO_STORE = ['bestScore', 'gridSize'];
