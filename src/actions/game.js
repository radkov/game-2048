import {
  START_GAME,
  NEW_TILE,
  USER_TURN,
  UPDATE_BOARD,
  USER_WON,
  USER_LOSE,
  USER_CONTINUE_PLAYING,
  REPLACE_BOARD,
  CHANGE_GRID_SIZE
} from '../constants/actionTypes';
import { generateNewTile, checkForUserWin, checkForPossibleMove, handleUserInput } from '../lib/game';
import { GAME_STATES } from '../constants/game';

/**
 * @description Action creator for indicating that a new game is starting
 */
export function startGame() {
  return {
    type: START_GAME
  };
}

/**
 * @description Action creator for indicating  that
 * a new tile is created and need to be added in the board
 * @param {object} tile
 */
export function newTile(tile) {
  return {
    type: NEW_TILE,
    payload: tile
  };
}

/**
 * @description Action creator indicating that user has interacted with the board and made his move
 * @param {string} direction The direction where user make. Values are LEFT, RIGHT, UP and DOWN
 */
export function userTurn(direction) {
  return {
    type: USER_TURN,
    payload: {
      direction
    }
  };
}

/**
 * @description Action creator for indicating that a board needs to be updated
 * @param {string[]} matrix Represents the game board
 * @param {Object} tiles Represents the tiles on the board
 * @param {number} additionalScore The score user get from current turn
 */
export function updateBoard(matrix, tiles, additionalScore) {
  return {
    type: UPDATE_BOARD,
    payload: {
      matrix,
      additionalScore,
      tiles
    }
  };
}

/**
 * @description Action creator for indicating that user won the game
 */
export function userWon() {
  return {
    type: USER_WON
  };
}

/**
 * @description Action creator for indicating that user lost the game
 */
export function userLose() {
  return {
    type: USER_LOSE
  };
}

/**
 * @description Action creator for indicating that user want to continue playing after his win
 */
export function userContinuePlaying() {
  return {
    type: USER_CONTINUE_PLAYING
  };
}

/**
 * @description Action creator for indicating that the board needs to be completely replaced.
 * Used for restoring previous and next states of the board.
 */
export function replaceBoard(payload) {
  return {
    type: REPLACE_BOARD,
    payload
  };
}

/**
 * @description Return a function that generate new tile and dispatch the action for it
 */
const dispatchNewTile = async (dispatch, getState) => {
  let state = getState();
  const tile = generateNewTile(state.game);
  if (!tile) {
    return;
  }
  return await dispatch(newTile(tile));
};

/**
 * @description Return a function that start a new game and generate two tiles for the new board
 */
export const loadNewGame = () => async (dispatch, getState) => {
  await dispatch(startGame());
  await dispatchNewTile(dispatch, getState);
  await dispatchNewTile(dispatch, getState);
};

/**
 * @description Return a function that handles user move
 * @param {function} dispatch
 * @param {function} getState
 */
export const handleTurn = (direction) => async (dispatch, getState) => {
  await dispatch(userTurn(direction));
  let state = getState();
  const { matrix, tiles } = state.game;
  const result = handleUserInput(direction, matrix, tiles);

  if (result && result.hasChanged) {
    await dispatch(updateBoard(result.matrix, result.tiles, result.additionalScore));
    state = getState();
  }

  await dispatchNewTile(dispatch, getState);
  state = getState();

  const hasUserWon = checkForUserWin(state.game.tiles);

  if (hasUserWon && state.game.gameState !== GAME_STATES.CONTINUE_PLAYING) {
    await dispatch(userWon());
  } else {
    const hasMoves = checkForPossibleMove(state.game.matrix, state.game.tiles);

    if (!hasMoves) {
      await dispatch(userLose());
    }
  }
};

/**
 * @description Action creator for indicating that the size of the board needs to change
 * @param {number} size
 */
export function changeGridSize(size) {
  return {
    type: CHANGE_GRID_SIZE,
    payload: {
      size
    }
  };
}

/**
 * @description Return a function that notifies for board size change
 * @param {number} size
 */
export const startGameWithNewGrid = (size) => async (dispatch, getState) => {
  await dispatch(changeGridSize(size));
  await dispatch(loadNewGame());
};
