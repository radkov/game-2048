import {
  INITIAL_LOAD,
  LOAD_SETTINGS
} from '../constants/actionTypes';
import { loadGameHistory } from './history';
import { GAME_SETTINGS } from '../constants/localStorage';
import { getItem } from '../lib/storageManager';

/**
 * @description Action creator for indicating that a board data is loading for the first time
 */
export function initialLoad() {
  return {
    type: INITIAL_LOAD
  };
}

/**
 * @description Action creator for loading game settings
 * @param {Object} payload Represents the settings
 * @param {number} payload.gridSize The size of the grid
 * @param {number} payload.bestScore User's best score
 */
export function loadSettings(payload) {
  return {
    type: LOAD_SETTINGS,
    payload,
  };
}

/**
 * @description Return a function that gets the user settings from the storage
 * and notify to load them in the state
 */
export const loadGameSettings = () => async (dispatch) => {
  const storedSettings = getItem(GAME_SETTINGS);
  if (storedSettings) {
    dispatch(loadSettings(storedSettings));
  }
};

/**
 * @description Return a function that restores all user settings and game history
 */
export const initialize = () => async (dispatch) => {
  await dispatch(initialLoad());
  await dispatch(loadGameHistory());
  await dispatch(loadGameSettings());
};
