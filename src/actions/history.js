import {
  STORE_STATE,
  REDO_STEP,
  UNDO_STEP,
  LOAD_STATES
} from '../constants/actionTypes';
import { GAME_HISTORY } from '../constants/localStorage';
import { replaceBoard } from './game';
import { getItem } from '../lib/storageManager';

/**
 * @description Action creator for indicating that a new state of the store needs
 * to be saved
 */
export function storeState(payload) {
  return {
    type: STORE_STATE,
    payload
  };
}

/**
 * @description Action creator for restoring all stored states
 */
export function loadStates(payload) {
  return {
    type: LOAD_STATES,
    payload
  };
}

/**
 * @description Action creator for indicating undo of a turn
 */
export function undoStep() {
  return {
    type: UNDO_STEP
  };
}

/**
 * @description Action creator for indicating redo of a turn
 */
export function redoStep() {
  return {
    type: REDO_STEP
  };
}

/**
 * @description Return a function that loads all stored states and notifies to replace the current one
 * @param {number} size
 */
export const loadGameHistory = () => async (dispatch, getState) => {
  const storedHistory = getItem(GAME_HISTORY);
  await dispatch(loadStates(storedHistory));
  const state = getState();
  const history = state.history;
  dispatch(replaceBoard(history.states[history.currentState]));
};
