import {
  createStore,
  compose,
  applyMiddleware,
} from 'redux';
import thunk from 'redux-thunk';

import rootReducer from '../reducers';
import gameSettings from '../middlewares/gameSettings';
import gameHistory from '../middlewares/gameHistory';

/**
 * @description Configure and cretes the store for the redux
 * @param {Object} initialState
 */
export default function configureStore(initialState) {
  const middewares = [
    thunk,
    gameSettings,
    gameHistory
  ];

  return createStore(rootReducer, initialState, compose(
    applyMiddleware(...middewares),
    window.devToolsExtension ? window.devToolsExtension() : f => f
  ));
}
