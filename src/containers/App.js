import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';

import '../styles/App.css';
import Board from './Board';
import * as gameActions from '../actions/game';
import * as initializeActions from '../actions/initialize';
import * as historyActions from '../actions/history';
import Score from '../components/Score';
import Button from '../components/Button';
import { keyboardArrowCodes } from '../constants/directions';
import { GRID_SIZE_4, GRID_SIZE_6, GRID_SIZE_5 } from '../constants/game';

/**
 * App is the root component for the game
 */
class App extends Component {
  ref = null;
  static propTypes = {
    initializeActions: PropTypes.objectOf(PropTypes.func).isRequired,
    gameActions: PropTypes.objectOf(PropTypes.func).isRequired,
    historyActions: PropTypes.objectOf(PropTypes.func).isRequired,
    currentScore: PropTypes.number.isRequired,
    bestScore: PropTypes.number.isRequired,
    gridSize: PropTypes.number.isRequired,
  }

  componentDidMount() {
    const { initialize } = this.props.initializeActions;
    initialize();
  }

  componentDidUpdate() {
    if (this.ref) {
      this.ref.focus();
    }
  }

  setRef = (ref) => {
    this.ref = ref;
  }

  handleUserInteraction = (event) => {
    if (keyboardArrowCodes[event.keyCode]) {
      const direction = keyboardArrowCodes[event.keyCode];
      this.props.gameActions.handleTurn(direction);
    }
  }

  render() {
    const { currentScore, bestScore, gridSize } = this.props;
    const { loadNewGame, startGameWithNewGrid } = this.props.gameActions;
    const { undoStep, redoStep } = this.props.historyActions;

    return (
      <div className="wrapper"
        onKeyDown={this.handleUserInteraction}
        ref={this.setRef}
        tabIndex={0}>
        <div className="app">
          <header className="app-header">
            <h1 className="app-title">GAME 2048</h1>
            <div className="score-container">
              <Score title="Current Score" value={currentScore} />
              <Score title="Best Score" value={bestScore} />
            </div>
          </header>
          <div className="menu">
            <Button onClick={undoStep} text="< Undo" />
            <Button onClick={loadNewGame} text="New Game" />
            <Button onClick={redoStep} text="Redo >" />
          </div>
          <Board gridSize={gridSize} />
          <div className="menu">
            <Button onClick={() => startGameWithNewGrid(GRID_SIZE_4)} text="4 x 4" />
            <Button onClick={() => startGameWithNewGrid(GRID_SIZE_5)} text="5 x 5" />
            <Button onClick={() => startGameWithNewGrid(GRID_SIZE_6)} text="6 x 6" />
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    currentScore: state.game.currentScore,
    bestScore: state.game.bestScore,
    gridSize: state.game.gridSize,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    gameActions: bindActionCreators(gameActions, dispatch),
    initializeActions: bindActionCreators(initializeActions, dispatch),
    historyActions: bindActionCreators(historyActions, dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);
