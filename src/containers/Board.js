import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import '../styles/Board.css';
import Row from '../components/Row';
import { createReactArray } from '../lib/array';
import * as gameActions from '../actions/game';
import TilesWrapper from './TilesWrapper';
import Notifier from '../components/Notifier';
import { WON, LOSE } from '../constants/game';
import Button from '../components/Button';

/**
 * Board contains the gaming/interactive part of the game
 */
class Board extends Component {
  static propTypes = {
    gameState: PropTypes.string.isRequired,
    handleTurn: PropTypes.func.isRequired,
    userContinuePlaying: PropTypes.func.isRequired,
    loadNewGame: PropTypes.func.isRequired,
    gridSize: PropTypes.number.isRequired,
  }

  render() {
    const gridSize = this.props.gridSize;
    const grid = createReactArray(this.props.gridSize, Row, { size: gridSize });
    const { gameState, loadNewGame, userContinuePlaying } = this.props;
    const newGameButton = <Button onClick={loadNewGame} text="New Game" />;

    return (
      <div
        className="board"
      >
        <TilesWrapper />
        {grid}
        {gameState === WON &&
          <Notifier message="Congrats! You WON :) Do you want to continue playing?">
            {newGameButton}
            <Button onClick={userContinuePlaying} text="Continue Playing" />
          </Notifier>
        }
        {gameState === LOSE &&
          <Notifier message="Game over! Do you want to try again?">
            {newGameButton}
          </Notifier>
        }
      </ div>
    );
  }
}

function mapStateToProps(state) {
  return {
    gameState: state.game.gameState
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(gameActions, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Board);

