import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import ReactEventComponent from 'react-swipe-event-component';

import '../styles/Board.css';
import { LEFT, RIGHT, UP, DOWN } from '../constants/directions';
import * as gameActions from '../actions/game';
import Tile from '../components/Tile';
import { HIDDEN } from '../lib/Tile';

/**
 * TilesWrapper contains tiles in the current board
 */
class TilesWrapper extends ReactEventComponent {
  handleSwipeLeft() {
    this.props.handleTurn(LEFT);
  }
  handleSwipeRight() {
    this.props.handleTurn(RIGHT);
  }
  handleSwipeUp() {
    this.props.handleTurn(UP);
  }
  handleSwipeDown() {
    this.props.handleTurn(DOWN);
  }

  render() {
    const { tiles, gridSize } = this.props;
    return (
      <div className="board-tile-container"
        {...this.touchEventProperties}
      >
        <div className="tiles-wrapper">
          {tiles.map((t, index) => (
            <Tile
              col={t.col}
              id={t.id}
              key={index}
              previousPosition={t.previousPosition}
              row={t.row}
              size={gridSize}
              state={t.state}
              transitionCol={t.transitionCol}
              transitionRow={t.transitionRow}
              value={t.value} />
          ))}
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  const { tiles } = state.game;
  let mappedTiles = Object.values(tiles).filter(t => t.state !== HIDDEN).slice();

  mappedTiles = mappedTiles.map(t => {
    if (t.mergeTo) {
      t.transitionRow = tiles[t.mergeTo].row;
      t.transitionCol = tiles[t.mergeTo].col;
    }

    return t;
  });

  return {
    tiles: mappedTiles,
    gridSize: state.game.gridSize
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(gameActions, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TilesWrapper);

