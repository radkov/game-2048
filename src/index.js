import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';

import './styles/index.css';
import App from './containers/App';
import registerServiceWorker from './lib/registerServiceWorker';
import configureStore from './store/configureStore';

const store = configureStore();

const Root = () => {
  return (
    <Provider store={store}>
      <App/>
    </Provider>
  );
};

ReactDOM.render(<Root />, document.getElementById('root'));
registerServiceWorker();
