import { GAME_SETTINGS, PROPS_TO_STORE, MODIFYING_ACTIONS } from '../constants/localStorage';
import { setItem } from '../lib/storageManager';

/**
 * @description Checks if two objects are based only on some properties
 * @param {Object} first
 * @param {Object} second
 * @param {Object} properties
 */
const isEqual = (first, second, properties) => {
  for (let property of properties) {
    if (first[property] !== second[property]) {
      return false;
    }
  }

  return true;
};

/**
 * @description Pick some of the properties of an object
 * @param {Object} item
 * @param {Object} properties
 */
const pluckProps = (item, properties) => {
  let newItem = {};
  for (let property of properties) {
    newItem[property] = item[property];
  }

  return newItem;
};

/**
 * Game settings middleware.
 * @description Used for storing user settings on certain actions
 */
const gameSettings = store => next => async action => {
  const prevState = store.getState();
  const actionResult = next(action);
  const nextState = store.getState();

  if (MODIFYING_ACTIONS.indexOf(action.type) >= 0) {
    const settingsChanged = !isEqual(prevState.game, nextState.game, PROPS_TO_STORE);

    if (settingsChanged) {
      setItem(GAME_SETTINGS, pluckProps(nextState.game, PROPS_TO_STORE));
    }
  }

  return actionResult;
};

export default gameSettings;
