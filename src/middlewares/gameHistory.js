import { GAME_HISTORY } from '../constants/localStorage';
import { START_GAME, UPDATE_BOARD, UNDO_STEP, REDO_STEP } from '../constants/actionTypes';
import { storeState } from '../actions/history';
import { replaceBoard } from '../actions/game';
import { setItem, removeItem } from '../lib/storageManager';

/**
 * @description Check if the action could be executed
 */
const checkForInterruptAction = (store, action) => {
  const prevState = store.getState();
  const prevHistory = prevState.history;
  if (action.type === UNDO_STEP) {
    if (prevHistory.currentState === 0 || prevHistory.states.length === 0) {
      return true;
    }
  }

  if (action.type === REDO_STEP) {
    if (prevHistory.currentState === prevHistory.states.length - 1 || prevHistory.states.length === 0) {
      return true;
    }
  }
};

/**
 * Game history middleware.
 * @description Used for control the history of the game state
 */
const gameHistory = store => next => async action => {
  if (checkForInterruptAction(store, action)) {
    return;
  }

  let actionResult = next(action);
  let nextState = store.getState();
  const history = nextState.history;

  if (action.type === UNDO_STEP) {
    if (history.currentState > 0) {
      store.dispatch(replaceBoard(history.states[history.currentState]));
    }
  } else if (action.type === REDO_STEP) {
    if (history.currentState < history.states.length) {
      store.dispatch(replaceBoard(history.states[history.currentState]));
    }
  } else if (action.type === UPDATE_BOARD) {
    store.dispatch(storeState(nextState.game));
    nextState = store.getState();
    setItem(GAME_HISTORY, nextState.history);
  } else if (action.type === START_GAME) {
    removeItem(GAME_HISTORY);
  }

  return actionResult;
};

export default gameHistory;
