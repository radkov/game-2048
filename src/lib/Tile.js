import uuid from 'uuid/v4';

export const NEW = 'new';
export const HIDDEN = 'hidden';
export const MERGED = 'merged';
export const NORMAL = 'normal';
export const INCREASED = 'increased';

/**
 * @description Creates a new tile based on a certain arguments
 * @param {number} row
 * @param {number} col
 * @param {number} value
 * @param {string} state
 * @param {string} mergeTo
 * @param {Object} prevousPosition
 * @return {Object} The created tile object
 */
export const createTile = (row, col, value = 2, state = NEW, mergeTo = null, previousPosition = null) => {
  const tile = {
    id: uuid(),
    row,
    col,
    value,
    state,
    mergeTo,
    previousPosition
  };

  return Object.assign({}, tile);
};
