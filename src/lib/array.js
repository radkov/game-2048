import React, { Fragment } from 'react';

import { ROW, LEFT } from '../constants/directions';
import { createTile, MERGED, INCREASED } from './Tile';

/**
 * @description Creates an array with fixed size of react components and return them in a React.Fragment
 * @param {number} size The size of the array
 * @param {React.Component} Element React.Component that will be used
 * @param {Object} props Porperties for the Element. Default value is empty object
 */
export const createReactArray = (size, Element, props = {}) => {
  const items = new Array(size);

  for (let i = 0; i < size; i++) {
    items[i] = <Element key={props.id || i} {...props} />; // eslint-disable-line
  }

  return (
    <Fragment>
      {items}
    </Fragment>
  );
};

/**
 * @description Creates an array of arrays (matrix) with fixed size and fill the elements with null values
 * @param {number} size The size of the array
 */
export const createMatrix = (size) => {
  const matrix = new Array(size);

  for (let i = 0; i < size; i++) {
    matrix[i] = new Array(size).fill(null);
  }

  return matrix;
};

/**
 * @description Try to merge equal elements in a matrix with the same value
 * @param {Object} array Special matrix representing one row or col of it
 * @param {Object} tiles Object containing the tiles currently on the board
 * @param {string} direction Represents the direction for the merge. Values are LEFT and RIGHT
 * @returns {Object} Indicates if the there was a change in the matrix and the score for it
 */
const tryToMergeTiles = (array, tiles, direction) => {
  let mergeIndex = Number.MIN_VALUE;
  let hasChanged = false;
  let result = 0;
  let i;
  if (direction === LEFT) {
    for (i = 0; i < array.length; i++) {
      mergeLogic();
    }
  } else {
    for (i = array.length - 1; i >= 0; i--) {
      mergeLogic();
    }
  }

  function mergeLogic() {
    if (array.getElementAt(i) === null) {
      return;
    } else if (mergeIndex === Number.MIN_VALUE) {
      mergeIndex = i;
      return;
    }

    const mergeTileId = array.getElementAt(mergeIndex);
    const currentTileId = array.getElementAt(i);
    const mergeTile = tiles[mergeTileId];
    const currentTile = tiles[currentTileId];

    if (!mergeTile || (mergeTile && currentTile && mergeTile.value !== currentTile.value)) {
      mergeIndex = i;
      return;
    } else {
      let newTile;
      const arrayIndex = array.lineIndex;
      const newTileValue = mergeTile.value * 2;

      if (array.direction === ROW) {
        newTile = createTile(arrayIndex, mergeIndex, newTileValue, INCREASED);
      } else {
        newTile = createTile(mergeIndex, arrayIndex, newTileValue, INCREASED);
      }

      mergeTile.state = MERGED;
      mergeTile.mergeTo = newTile.id;
      currentTile.state = MERGED;
      currentTile.mergeTo = newTile.id;

      tiles[newTile.id] = newTile;
      result += newTile.value;

      array.setElementAt(mergeIndex, newTile.id);
      array.setElementAt(i, null);

      hasChanged = true;
      direction === LEFT ? i++ : i--;
      mergeIndex = i;
    }
  }

  return {
    hasChanged,
    result
  };
};

/**
 * @description Try to move elements to the edge of empty placeholder in a matrix
 * @param {Object} array Special matrix representing one row or col of it
 * @param {Object} tiles Object containing the tiles currently on the board
 * @param {string} direction Represents the direction for the merge. Values are LEFT and RIGHT
 * @returns {Object} Indicates if the there was a change in the matrix
 */
const tryToMoveTiles = (array, tiles, direction) => {
  let hasChanged = false;
  let i, j;
  const arrayLength = array.length;

  if (direction === LEFT) {
    for (i = 0; i < arrayLength - 1; i++) {
      if (!array.getElementAt(i)) {
        for (j = i + 1; j < arrayLength; j++) {
          if (moveLogic()) { break; }
        }
      }
    }
  } else {
    for (i = arrayLength - 1; i >= 1; i--) {
      if (!array.getElementAt(i)) {
        for (j = i - 1; j >= 0; j--) {
          if (moveLogic()) { break; }
        }
      }
    }
  }

  function moveLogic() {
    if (array.getElementAt(j)) {
      array.setElementAt(i, array.getElementAt(j));
      array.setElementAt(j, null);
      const movedId = array.getElementAt(i);
      const arrayIndex = array.lineIndex;

      if (array.direction === ROW) {
        tiles[movedId].previousPosition = {
          row: tiles[movedId].row,
          col: tiles[movedId].col
        };
        tiles[movedId].row = arrayIndex;
        tiles[movedId].col = i;
      } else {
        tiles[movedId].previousPosition = {
          row: tiles[movedId].row,
          col: tiles[movedId].col
        };
        tiles[movedId].row = i;
        tiles[movedId].col = arrayIndex;
      }

      hasChanged = true;
      return true;
    }
  }

  return {
    hasChanged
  };
};

/**
 * @description Wrap the logic for
 * @param {Object} array Special matrix representing one row or col of it
 * @param {Object} tiles Object containing the tiles currently on the board
 * @param {string} direction Represents the direction for the merge. Values are LEFT and RIGHT
 * @returns {Object} Indicates if the there was a change in the matrix, the score for it and the matrix itself
 */
export const moveTiles = (array, tiles, direction) => {
  let mergeResult = tryToMergeTiles(array, tiles, direction);
  let moveResult = tryToMoveTiles(array, tiles, direction);

  return {
    result: mergeResult.result,
    matrix: array.toMatrix(),
    hasChanged: mergeResult.hasChanged || moveResult.hasChanged
  };
};
