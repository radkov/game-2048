/**
 * @description Get item from localStorage
 * @param {string} key
 * @return {Object} The JSON.parse object from the localStorage
 */
export const getItem = (key) => {
  const item = JSON.parse(localStorage.getItem(key));

  return item;
};

/**
 * @description Set item to localStorage. If the localStorage exceed the quota
 * remove the previous value and store the new one
 * @param {string} key
 * @param {Object} value
 */
export const setItem = (key, value) => {
  try {
    localStorage.setItem(key, JSON.stringify(value));
  } catch (e) {
    removeItem(key);
    setItem(key, value);
  }
};

/**
 * @description Remove item from localStorage
 * @param {string} key
 */
export const removeItem = (key) => {
  try {
    localStorage.removeItem(key);
  } catch (e) { }
};
