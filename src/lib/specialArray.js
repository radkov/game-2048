import { ROW } from '../constants/directions';

const getRowLength = (matrix, lineIndex) => () => matrix[lineIndex].length;
const getColLength = (matrix, lineIndex) => () => matrix.length;
const getRowElementAt = (matrix, lineIndex) => (index) => matrix[lineIndex][index];
const setRowElementAt = (matrix, lineIndex) => (index, value) => {
  matrix[lineIndex][index] = value;
};
const getColElementAt = (matrix, lineIndex) => (index) => matrix[index][lineIndex];
const setColElementAt = (matrix, lineIndex) => (index, value) => {
  matrix[index][lineIndex] = value;
};

/**
 * @description Create a specific one dimensional array from a Matrix
 * @param {string[]} matrix
 * @param {string} direction Values are LEFT and RIGHT
 * @param {number} lineIndex The index of a row or col that will use as the new array
 */
export const createOneDimensionalArrayFromMatrix = (matrix, direction, lineIndex) => {
  let getLength, getElementAt, setElementAt;

  if (direction === ROW) {
    getLength = getRowLength(matrix, lineIndex);
    setElementAt = setRowElementAt(matrix, lineIndex);
    getElementAt = getRowElementAt(matrix, lineIndex);
  } else {
    getLength = getColLength(matrix, lineIndex);
    setElementAt = setColElementAt(matrix, lineIndex);
    getElementAt = getColElementAt(matrix, lineIndex);
  }

  return {
    get length() {
      return getLength();
    },
    get direction() {
      return direction;
    },
    get lineIndex() {
      return lineIndex;
    },
    getElementAt,
    setElementAt,
    toMatrix() {
      return matrix;
    }
  };
};
