import { moveTiles } from './array';
import { generateRandomNumber } from './randomGenerator';
import { createTile, INCREASED } from './Tile';
import { WIN_NUMBER } from '../constants/game';
import { createOneDimensionalArrayFromMatrix } from './specialArray';
import { ROW, LEFT, RIGHT, COL, DOWN, UP } from '../constants/directions';

/**
 * @description Generates a new tile object in the empty space of the matrix.
 * If there is no space returns undefined
 * @param {Object} game Game state
 * @returns {Object|undefined} The created tile or undefined
 */
export const generateNewTile = (game) => {
  let position;
  let tileValue;

  let freeCells = [];

  game.matrix.forEach((row, i) => {
    row.forEach((col, j) => {
      if (!col) {
        freeCells.push({ row: i, col: j });
      }
    });
  });

  if (freeCells.length === 0) {
    return;
  }

  const freeCellIndex = generateRandomNumber(0, freeCells.length - 1);
  position = freeCells[freeCellIndex];
  tileValue = generateRandomNumber(1, 2) * 2;

  const tile = createTile(position.row, position.col, tileValue);
  return tile;
};

/**
 * @description Checking if the user has the winner number in his tiles and hasn't accepted the win
 * @param {Object} tiles
 * @returns {boolean}
 */
export const checkForUserWin = (tiles) => {
  let result = false;
  const values = Object.values(tiles);
  values.forEach(v => {
    if (v.state === INCREASED && v.value === WIN_NUMBER) {
      result = true;
    }
  });
  return result;
};

/**
 * @description Indicates if there is a possible move for the user.
 * Used to determine if the game is finished
 * @param {string[]} matrix
 * @param {Object} tiles
 * @returns {boolean}
 */
export const checkForPossibleMove = (matrix, tiles) => {
  const clonedMatrix = matrix.map((arr) => arr.slice());
  const clonedTiles = JSON.parse(JSON.stringify(tiles));
  let hasMoves = false;

  for (let index = 0; index < clonedMatrix.length; index++) {
    const leftMove = moveTiles(
      createOneDimensionalArrayFromMatrix(clonedMatrix, ROW, index),
      clonedTiles,
      LEFT
    );
    const rightMove = moveTiles(
      createOneDimensionalArrayFromMatrix(clonedMatrix, ROW, index),
      clonedTiles,
      RIGHT
    );
    const upMove = moveTiles(
      createOneDimensionalArrayFromMatrix(clonedMatrix, COL, index),
      clonedTiles,
      LEFT
    );
    const downMove = moveTiles(
      createOneDimensionalArrayFromMatrix(clonedMatrix, COL, index),
      clonedTiles,
      RIGHT
    );

    hasMoves = leftMove.hasChanged || rightMove.hasChanged || upMove.hasChanged || downMove.hasChanged;

    if (hasMoves) {
      break;
    }
  }

  return hasMoves;
};

/**
 * @description Handles the user move and calculate the new board and score for him
 * @param {string} direction
 * @param {string[]} matrix
 * @param {Object} tiles
 * @returns {Object}
 */
export const handleUserInput = (direction, matrix, tiles) => {
  let migrationResult = {
    additionalScore: 0,
    matrix: matrix.map((arr) => arr.slice()),
    hasChanged: false,
    tiles: JSON.parse(JSON.stringify(tiles))
  };

  migrationResult.matrix.forEach((arr, index) => {
    let processedLine;
    switch (direction) {
      case LEFT:
        processedLine = moveTiles(
          createOneDimensionalArrayFromMatrix(
            migrationResult.matrix,
            ROW,
            index
          ),
          migrationResult.tiles,
          LEFT
        );
        break;
      case RIGHT:
        processedLine = moveTiles(
          createOneDimensionalArrayFromMatrix(
            migrationResult.matrix,
            ROW,
            index
          ),
          migrationResult.tiles,
          RIGHT
        );
        break;
      case UP:
        processedLine = moveTiles(
          createOneDimensionalArrayFromMatrix(
            migrationResult.matrix,
            COL,
            index
          ),
          migrationResult.tiles,
          LEFT
        );
        break;
      case DOWN:
        processedLine = moveTiles(
          createOneDimensionalArrayFromMatrix(
            migrationResult.matrix,
            COL,
            index
          ),
          migrationResult.tiles,
          RIGHT
        );
        break;
    }

    if (processedLine && processedLine.hasChanged) {
      migrationResult.additionalScore += processedLine.result;
      migrationResult.matrix = processedLine.matrix;
      migrationResult.hasChanged = processedLine.hasChanged;
    }
  });

  return migrationResult;
};
