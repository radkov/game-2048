/**
 * @description Generate a random number in a range inclusive
 * @param {number} min
 * @param {number} max
 * @returns {Object} A random number between [min, max]
 */
export const generateRandomNumber = (min, max) => {
  return Math.floor(Math.random() * (max - min + 1)) + min;
};
