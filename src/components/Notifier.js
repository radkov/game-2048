import React from 'react';
import PropTypes from 'prop-types';

import '../styles/Notifier.css';

/**
 * Notifier component
 */
const Notifier = ({
  message,
  children
}) => {
  return (
    <div className="notifier">
      <h2>{message}</h2>
      <div className="notifier-buttons">
        {children}
      </div>
    </div>
  );
};

Notifier.propTypes = {
  message: PropTypes.string.isRequired,
  children: PropTypes.element.isRequired
};

export default Notifier;
