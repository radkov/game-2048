import React from 'react';
import PropTypes from 'prop-types';

import '../styles/Score.css';

/**
 * Score component
 */
const Score = ({
  title,
  value
}) => {

  return (
    <div className="score">
      <div className="score-title">{title}</div>
      <div className="score-value">{value}</div>
    </div>
  );
};

Score.propTypes = {
  title: PropTypes.string.isRequired,
  value: PropTypes.number.isRequired
};

export default Score;
