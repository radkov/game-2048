import React from 'react';
import PropTypes from 'prop-types';

import '../styles/Button.css';

/**
 * Button component
 */
const Button = ({
  onClick,
  text
}) => {
  return (
    <a className="button" onClick={onClick}>{text}</a>
  );
};

Button.propTypes = {
  onClick: PropTypes.func.isRequired,
  text: PropTypes.string.isRequired
};

export default Button;
