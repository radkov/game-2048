
import React from 'react';
import PropTypes from 'prop-types';

import '../styles/Tile.css';
import { MERGED } from '../lib/Tile';

/**
 * Tile component
 */
const Tile = ({
  id,
  value,
  row,
  col,
  state,
  transitionRow,
  transitionCol,
  previousPosition,
  size
  }) => {
  let stateClass = `state-${state}`;
  let positionClass = `position-${size}-${row}-${col}`;
  let moveClass;
  if (state === MERGED) {
    stateClass = `state-merged-from-${size}-${transitionRow}-${transitionCol}`;
  }
  if (previousPosition) {
    moveClass = `move-to-${size}-${row}-${col}`;
    positionClass = `position-${size}-${previousPosition.row}-${previousPosition.col}`;
  }

  const classes = `tile-${size} value-${value} ${moveClass} ${positionClass} ${stateClass} `;
  return (
    <div className={classes} key={id}>
      <span className="tile-value">
        {value}
      </span>
    </div>
  );
};

Tile.propTypes = {
  id: PropTypes.string.isRequired,
  value: PropTypes.number.isRequired,
  row: PropTypes.number.isRequired,
  col: PropTypes.number.isRequired,
  size: PropTypes.number.isRequired,
  state: PropTypes.string.isRequired,
  transitionRow: PropTypes.number,
  transitionCol: PropTypes.number,
  previousPosition: PropTypes.object,
};


export default Tile;
