import React from 'react';
import Placeholder from './Placeholder';
import PropTypes from 'prop-types';

import { createReactArray } from '../lib/array';
import '../styles/Row.css';

/**
 * Row component
 */
const Row = ({
  size = 4
}) => {
  const items = createReactArray(size, Placeholder, { size });

  return (
    <div className="row">
      {items}
    </div>
  );
};

Row.propTypes = {
  size: PropTypes.number.isRequired,
};

export default Row;
