
import React from 'react';
import PropTypes from 'prop-types';

import '../styles/Placeholder.css';

/**
 * Placeholder component
 */
const Placeholder = ({ size }) => {
  const className = `placeholder-${size}`;
  return (
    <div className={className} />
  );
};

Placeholder.propTypes = {
  size: PropTypes.number.isRequired,
};

export default Placeholder;
