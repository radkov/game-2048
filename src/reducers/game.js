import {
  START_GAME,
  NEW_TILE,
  UPDATE_BOARD,
  USER_WON,
  USER_LOSE,
  LOAD_SETTINGS,
  REPLACE_BOARD,
  USER_CONTINUE_PLAYING,
  CHANGE_GRID_SIZE
} from '../constants/actionTypes';
import { getInitialGameState } from './initialState';
import { GAME_STATES } from '../constants/game';
import { MERGED, HIDDEN, NEW, NORMAL, INCREASED } from '../lib/Tile';
const initialGameState = getInitialGameState();

/**
 * Game reducer
 */
export default (
  state = initialGameState,
  action
) => {
  switch (action.type) {
    case LOAD_SETTINGS:
      return {
        ...state,
        bestScore: action.payload.bestScore,
        gridSize: action.payload.gridSize
      };
    case START_GAME:
      const newGame = getInitialGameState(state.gridSize, state.bestScore);

      return {
        ...newGame
      };
    case USER_CONTINUE_PLAYING: {
      return {
        ...state,
        gameState: GAME_STATES.CONTINUE_PLAYING
      };
    }
    case NEW_TILE:
      const newMatrix = state.matrix.map((arr) => arr.slice());
      const { row, col, id } = action.payload;
      newMatrix[row][col] = action.payload.id;

      return {
        ...state,
        matrix: newMatrix,
        tiles: {
          ...state.tiles,
          [id]: action.payload
        }
      };
    case UPDATE_BOARD:
      const { additionalScore, tiles, matrix } = action.payload;
      const currentScore = state.currentScore + additionalScore;
      const bestScore = state.bestScore < currentScore ? currentScore : state.bestScore;

      const oldTiles = Object.values(state.tiles);
      const newTiles = Object.assign({}, tiles);
      oldTiles.forEach(t => {
        const nextTile = newTiles[t.id];
        if (t.state === MERGED) {
          delete newTiles[t.id];
        }
        if (t.state === MERGED) {
          nextTile.state = HIDDEN;
        }
        if (t.previousPosition) {
          if (!(nextTile.previousPosition
            && (nextTile.previousPosition.row !== t.previousPosition.row
              || nextTile.previousPosition.col !== t.previousPosition.col))) {
            nextTile.previousPosition = null;
          }
        }
        if (t.state === nextTile.state && (t.state === NEW || t.state === INCREASED)) {
          nextTile.state = NORMAL;
        }
      });

      let gameState = state.gameState === GAME_STATES.CONTINUE_PLAYING ? state.gameState : GAME_STATES.PLAYING;

      return {
        ...state,
        currentScore,
        matrix,
        gameState,
        tiles: newTiles,
        bestScore
      };
    case USER_WON:
      return {
        ...state,
        gameState: GAME_STATES.WON
      };
    case USER_LOSE:
      return {
        ...state,
        gameState: GAME_STATES.LOSE
      };
    case REPLACE_BOARD:
      return {
        ...state,
        ...action.payload
      };
    case CHANGE_GRID_SIZE:
      return {
        ...state,
        gridSize: action.payload.size
      };
    default:
      return state;
  }
};
