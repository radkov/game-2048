import {
  START_GAME,
  LOAD_STATES,
  STORE_STATE,
  UNDO_STEP,
  REDO_STEP
} from '../constants/actionTypes';
import { history } from './initialState';

/**
 * Game history reducer
 */
export default (
  state = history,
  action
) => {
  switch (action.type) {
    case START_GAME:
      return {
        ...history
      };
    case LOAD_STATES:
      return {
        ...state,
        ...action.payload
      };
    case STORE_STATE:
      const newStates = state.states.slice(0, state.currentState + 1);
      newStates.push(action.payload);

      return {
        ...state,
        currentState: newStates.length - 1,
        states: newStates
      };
    case UNDO_STEP:
      const newCurrentState = state.currentState - 1 < 0 ? 0 : state.currentState - 1;

      return {
        ...state,
        currentState: newCurrentState
      };
    case REDO_STEP:
      const currentState = state.currentState + 1 > state.states.length - 1 ? state.states.length - 1 : state.currentState + 1;

      return {
        ...state,
        currentState,
      };
    default:
      return state;
  }
};
