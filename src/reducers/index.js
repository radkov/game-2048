import { combineReducers } from 'redux';

import game from './game';
import history from './history';

/**
 * Combines all reducers
 */
export const rootReducer = combineReducers({
  game,
  history
});

export default rootReducer;
