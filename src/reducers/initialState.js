import {
  INITIAL_SCORE,
  GRID_SIZE
} from '../constants/initialStateValues';
import {
  STARTED
} from '../constants/game';
import { createMatrix } from '../lib/array';

/**
 * Initial state for the game
 */
export const getInitialGameState = (gridSize = GRID_SIZE, bestScore = INITIAL_SCORE) => ({
  currentScore: INITIAL_SCORE,
  bestScore,
  gridSize,
  matrix: createMatrix(gridSize),
  tiles: {},
  gameState: STARTED
});

/**
 * Initial state for the history
 */
export const history = {
  currentState: 0,
  states: []
};
